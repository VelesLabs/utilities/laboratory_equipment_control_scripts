#!/usr/bin/env python3

import serial
import time
import argparse
import sys

baud=9600;
#device='/dev/lab_source_manson_sdp2603';
#device='';
class manson_sdp2603:
    def __init__(self, device):
        self.device = device

    @staticmethod
    def serial_send_command(device, command):
        ser = serial.Serial(port=device, baudrate=baud, timeout=0.1, write_timeout=0.1);
        ser.close();
        ser.open();
        ser.write(command.encode('utf-8')); 
        status = ser.read(3);
        ser.close();
        if status == 'OK\r'.encode('utf-8'):
            return 0;
        else:
            return -1;

    def get_voltage_current_mode(self):
        ser = serial.Serial(port=self.device, baudrate=baud, timeout=0.1, write_timeout=0.1);
        ser.close();
        ser.open();
        command="GETD__\r"
        ser.write(command.encode('utf-8')); 
        read_values = ser.read_until(b"\r");
        status = ser.read(3);
        ser.close();
        if status == 'OK\r'.encode('utf-8'):
            voltage = int(read_values[0:4])
            current = int(read_values[4:8]);
            mode = int(read_values[8]);
            return 0,voltage,current,mode;
        else:
            return -1,-1,-1,-1;

    def output_enable(self):
        return self.serial_send_command(self.device, "SOUT__0\r");

    def output_disable(self):
        return self.serial_send_command(self.device, "SOUT__1\r");

    def set_voltage( self, voltage ):
        if (1 <= voltage) and (voltage <= 60): 
            command = "VOLT__"+(str(int(voltage*10)).zfill(3))[0:3]+"\r";
            return self.serial_send_command(self.device, command);
        else:
            return -5;

    def set_max_current( self, current ):
        if (0.01 <= current) and (current <= 3.3): 
            command = "CURR__"+(str(int(current*100)).zfill(3))[0:3]+"\r";
            return self.serial_send_command(self.device, command);  
        else:
            return -5;

    def screen_enable(self):
        return self.serial_send_command(self.device, "ENDS__\r");

    def screen_disable(self):
        return self.serial_send_command(self.device, "SESS__\r");

    def print_help(self):
        print("HELP");

def main():
    # Construct the argument parser
    ap = argparse.ArgumentParser();

    # Add the arguments to the parser
    ap.add_argument("--device", required=True, help="Serial Device");
    #ap.add_argument("--help", required=False, help="Help");
    ap.add_argument("-oe", "--output_enable", required=False, help="Output Enable", action="store_true");    
    ap.add_argument("-od", "--output_disable", required=False, help="Output Disable", action="store_true");     
    ap.add_argument("-se", "--screen_enable", required=False, help="Screen Enable", action="store_true");    
    ap.add_argument("-sd", "--screen_disable", required=False, help="Screen Disable", action="store_true");    
    ap.add_argument("-vs", "--voltage_set", required=False, help="Set Voltage", type=float);  
    ap.add_argument("-cs", "--current_set", required=False, help="Set Current", type=float);  
    ap.add_argument("-vg", "--voltage_get", required=False, help="Get reading - Voltage", action="store_true");
    args = (ap.parse_args());
    
    global device;
    sdp2603 = manson_sdp2603(args.device);

    if args.output_enable:
        return sdp2603.output_enable();
    
    if args.output_disable:
        return sdp2603.output_disable();

    if args.screen_enable:
        return sdp2603.screen_enable();
    
    if args.screen_disable:
        return sdp2603.screen_disable();
    
    if str(args.voltage_set) != "None":
        return sdp2603.set_voltage(args.voltage_set);
        
    if str(args.current_set) != "None":
        return sdp2603.set_max_current(args.current_set);
    
    if args.voltage_get:
        ret, volt, curr, mode = sdp2603.get_voltage_current_mode();      
        return volt;

if __name__=="__main__":
    main();
